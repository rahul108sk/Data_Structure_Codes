#include<bits/stdc++.h>
using namespace std;

struct node
{
    int data;
    struct node *next;
};
struct node *head;
void insert(int data,int n)
{
    node *temp1 = new node();
    temp1->data = data;
    temp1->next = NULL;
    if(n==1){
        temp1->next = head;
        head = temp1;
        return;
    }
    node *temp2 = head;
    for(int i=0;i<n-2;i++)
        temp2=temp2->next;

        temp1->next = temp2->next;
        temp2->next = temp1;
}
void print()
{
    node *temp = head;
    while(temp != NULL){
        printf("%d ",temp->data);
        temp = temp->next;
    }
    printf("\n");
}
main()
{
    /*
    head = NULL;
    insert(2,1);
    insert(3,2);
    insert(4,1);
    insert(5,2);
    print();
    */

    int input,loc;
    int t;
    cin>>t;
    for(int i=0;i<t;i++){
    cin>>input>>loc;
    insert(input,loc);
    print();
    }

}
